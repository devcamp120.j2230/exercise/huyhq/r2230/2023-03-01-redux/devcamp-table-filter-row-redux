import { TASK_BUTTON_FILTER, TASK_INPUT_CHANGE } from "../constants/task.const";

//các state sẽ thay đổi 
const taskState = {
    inputValue: "",
    tableFilter: [
        {tableValue: "Cà phê"},
        {tableValue: "Trà tắc"},
        {tableValue: "Pepsi"},
        {tableValue: "Cocacola"},
        {tableValue: "Hồng trà"},
        {tableValue: "Trà sữa"},
    ]
};

const taskReducer = (state = taskState, action)=>{
    switch (action.type) {
        case TASK_INPUT_CHANGE:
            state.inputValue = action.payload
            break;
        case TASK_BUTTON_FILTER:
            state.tableFilter = taskState.tableFilter.filter((element)=>{
                var val1 = element.tableValue.toLowerCase();
                var val2 = state.inputValue.toLowerCase();
                return val1.includes(val2);
            });
            break;
        default:
            break;
    }
    return {...state};
};

export default taskReducer;