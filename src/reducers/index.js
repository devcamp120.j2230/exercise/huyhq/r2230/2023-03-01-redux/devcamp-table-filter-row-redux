import { combineReducers } from "redux";
import taskReducer from "./task.reducer";

//root chứa các task reducer 
const rootReducer = combineReducers({
    taskReducer
}); 

export default rootReducer;