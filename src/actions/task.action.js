import { TASK_BUTTON_FILTER, TASK_INPUT_CHANGE } from "../constants/task.const"

const inputChangeHandler = (inputValue)=>{
    return {
        type: TASK_INPUT_CHANGE,
        payload: inputValue
    };
};

const buttonFilterHandler = ()=>{
    return {
        type: TASK_BUTTON_FILTER
    };
};

export {
    inputChangeHandler,
    buttonFilterHandler
}