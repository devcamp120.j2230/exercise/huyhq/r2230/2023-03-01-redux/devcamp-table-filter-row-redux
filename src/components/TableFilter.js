import { Container, Grid, TextField, Button, Table, TableHead, TableCell, TableRow, TableBody  } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { buttonFilterHandler, inputChangeHandler } from "../actions/task.action";

const TableFilterShow = ()=>{
    //cho phép lấy giá trị từ view đưa về reducer
    const dispatch = useDispatch();
    
    //lấy giá trị từ reducer
    const { inputValue, tableFilter } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    });


    const onInputTextChange = (e)=>{
        dispatch(inputChangeHandler(e.target.value));
    };

    const onButtonClickFilter = ()=>{
        dispatch(buttonFilterHandler());
    };

    return(
        <Container>
            <Grid container mt={5} alignItems="center" py={5}>
                <Grid item xs={12} md={6} lg={8}>
                    <TextField label="Nhap noi dung ..." variant="outlined" fullWidth onChange={onInputTextChange} value={inputValue}/>
                </Grid>
                <Grid item xs={12} md={6} lg={4} textAlign="center">
                    <Button variant="contained" onClick={onButtonClickFilter}>Them</Button>
                </Grid>
            </Grid>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Stt</TableCell>
                        <TableCell>Noi dung</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {tableFilter.map((value, i)=>{
                        return <TableRow key={i}>
                                    <TableCell>{i+1}</TableCell>
                                    <TableCell>{value.tableValue}</TableCell>
                                </TableRow>
                    })}
                </TableBody>
            </Table>
        </Container>
    );
};

export default TableFilterShow;